package ru.krivotulov.tm.api.service;

import ru.krivotulov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    Task create(String name);

    Task create(String name, String description);

    Task add(Task task);

    List<Task> findAll();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task delete(Task task);

    Task deleteById(String id);

    Task deleteByIndex(Integer index);

    void clear();

}
