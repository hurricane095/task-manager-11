package ru.krivotulov.tm.api.controller;

public interface ITaskController {

    void displayTaskList();

    void clearTasks();

    void createTask();

    void removeTaskByIndex();

    void removeTaskById();

    void showTaskByIndex();

    void showTaskById();

    void updateTaskByIndex();

    void updateTaskById();

}
