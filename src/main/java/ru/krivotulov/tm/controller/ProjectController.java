package ru.krivotulov.tm.controller;

import ru.krivotulov.tm.api.controller.IProjectController;
import ru.krivotulov.tm.api.service.IProjectService;
import ru.krivotulov.tm.model.Project;
import ru.krivotulov.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void displayProjectList() {
        System.out.println("[PROJECT LIST]");
        final List<Project> projectList = projectService.findAll();
        for (Project project : projectList) {
            if (project == null) continue;
            System.out.println(project);
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[PROJECT CLEAR]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.readLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.readLine();
        final Project project = projectService.create(name, description);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.deleteByIndex(index);
        if (project == null) System.out.println("FAIL");
        else System.out.println("OK");
    }

    @Override
    public void removeProjectById() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.readLine();
        final Project project = projectService.deleteById(id);
        if (project == null) System.out.println("FAIL");
        else System.out.println("OK");
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) {
            System.out.println("FAIL");
            return;
        }
        showProject(project);
        System.out.println("OK");
    }

    @Override
    public void showProjectById() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.readLine();
        final Project project = projectService.findOneById(id);
        if (project == null) {
            System.out.println("FAIL");
            return;
        }
        showProject(project);
        System.out.println("OK");
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.readLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.readLine();
        final Project project = projectService.updateByIndex(index, name, description);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        System.out.println("OK");
    }

    @Override
    public void updateProjectById() {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.readLine();
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.readLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.readLine();
        final Project project = projectService.updateById(id, name, description);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        System.out.println("OK");
    }

    private void showProject(Project project) {
        if (project == null) return;
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("ID: " + project.getId());
    }

}
