package ru.krivotulov.tm.controller;

import ru.krivotulov.tm.api.controller.ITaskController;
import ru.krivotulov.tm.api.service.ITaskService;
import ru.krivotulov.tm.model.Task;
import ru.krivotulov.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void displayTaskList() {
        System.out.println("[TASK LIST]");
        final List<Task> taskList = taskService.findAll();
        int index = 1;
        for (Task task : taskList) {
            if (task == null) continue;
            System.out.println(index + "." + task);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.readLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.readLine();
        final Task task = taskService.create(name, description);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.deleteByIndex(index);
        if (task == null) System.out.println("FAIL");
        else System.out.println("OK");
    }

    @Override
    public void removeTaskById() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.readLine();
        final Task task = taskService.deleteById(id);
        if (task == null) System.out.println("FAIL");
        else System.out.println("OK");
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("FAIL");
            return;
        }
        showTask(task);
        System.out.println("OK");
    }

    @Override
    public void showTaskById() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.readLine();
        final Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println("FAIL");
            return;
        }
        showTask(task);
        System.out.println("OK");
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.readLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.readLine();
        final Task task = taskService.updateByIndex(index, name, description);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        System.out.println("OK");
    }

    @Override
    public void updateTaskById() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.readLine();
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.readLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.readLine();
        final Task task = taskService.updateById(id, name, description);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        System.out.println("OK");
    }

    private void showTask(Task task) {
        if (task == null) return;
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("ID: " + task.getId());
    }

}
